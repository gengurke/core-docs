# JTL-Shop Consent Manager

Der Consent Manager von JTL-Shop stellt die Verwaltung für die Einwilligungen betroffener Personen, hier
Endverbraucher, zur Weiterverarbeitung ihrer personenbezogenen Daten dar.

Seit dem EuGH-Urteil vom 01.10.2019 müssen Website-Betreiber im Rahmen der *Cookie-Richtlinie* über gesetzte Cookies
umfassend informieren und eine Einwilligung über die Verarbeitung personenbezogener Daten von ihren Website-Besuchern
einholen.

![](_images/cm-banner.png)

Mit dem JTL-Shop Consent Manager können die Einwilligungen ("Consents") der Website-Besucher unkompliziert erfragt
und abgespeichert werden.

![](_images/cm-banner_mark.png)

![](_images/cm-mainscreen.png)

Der Aufruf dieser Einstellungen ist im Frontend jederzeit über einen entsprechenden Button möglich:

![](_images/cm-icon.png)

## Consent Manager im Plugin

Auch Plugins können über den Consent Manager von JTL-Shop 5 Einverständniserklärungen einfordern.

Hierfür registriert ein Plugin über den EventDispatcher
("[Der EventDispatcher](../shop_plugins/bootstrapping.md#der-eventdispatcher)") einen Listener für das Event
`CONSENT_MANAGER_GET_ACTIVE_ITEMS`.

    $dispatcher->listen('shop.hook.' . \CONSENT_MANAGER_GET_ACTIVE_ITEMS, [$this, 'addConsentItem']);

Wird nun das Event `CONSENT_MANAGER_GET_ACTIVE_ITEMS` ausgelöst, registriert die Funktion
`addConsentItem` im Plugin die entsprechende Einverständniserklärung im JTL-Shop Consent Manager.

    /**
     * @param array $args
     */
    public function addConsentItem(array $args): void
    {
        $lastID = $args['items']->reduce(static function ($result, Item $item) {
                $value = $item->getID();

                return $result === null || $value > $result ? $value : $result;
            }) ?? 0;
        $item   = new Item();
        $item->setName('JTL Example Consent');
        $item->setID(++$lastID);
        $item->setItemID('jtl_test_consent');
        $item->setDescription('Dies ist nur ein Test aus dem Plugin JTL Test');
        $item->setPurpose('Dieser Eintrag dient nur zu Testzwecken');
        $item->setPrivacyPolicy('https://www.jtl-software.de/datenschutz');
        $item->setCompany('JTL-Software-GmbH');
        $args['items']->push($item);
    }

Zur Einforderung der Einverständniserklärung wird nun ein entsprechender Schalter im JTL-Shop Consent Manager
angezeigt.

![](_images/cm-testcons.png)

Per JavaScript kann man im Frontend die Einwilligung des Consent-Items wie folgt abfragen:

    CM.getSettings('consentId')

Bei erteilter Einwilligung gibt die Methode `true` zurück. `consentId` ist in dem Fall die per
`Item::setItemID()` vergebene ID.

Im Plugin "[jtl-test](https://gitlab.com/jtl-software/jtl-shop/plugins/jtl_test)" können Sie sich diese Vorgehensweise
in ausführlicherer Form anschauen.