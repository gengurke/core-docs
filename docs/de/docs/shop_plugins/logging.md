# Logging

Seit Version 5.3.0 haben Plugins die Möglichkeit, in eigene "Channels" zu loggen.
Diese können anschließend im Backend herausgefiltert werden und erhöhen die Übersichtlichkeit.

## Nutzung in der Bootstrap.php

An allen Instanzen von `\JTL\Plugin\PluginInterface` existiert die Methode `getLogger(): \Psr\Log\LoggerInterface`.
Ein einfaches Beispiel dazu könnte so aussehen:

    <?php declare(strict_types=1);

    namespace Plugin\example_plugin;

    use JTL\Events\Dispatcher;
    use JTL\Plugin\Bootstrapper;
    use JTL\Shop;

    class Bootstrap extends Bootstrapper
    {
        public function boot(Dispatcher $dispatcher): void
        {
            parent::boot($dispatcher);
            $dispatcher->hookInto(\HOOK_SMARTY_INC, function () {
                if (\method_exists($this->getPlugin(), 'getLogger')) {
                    $logger = $this->getPlugin()->getLogger();
                } else {
                    // fallback for shop versions < 5.3.0
                    $logger = Shop::Container()->getLogService();
                }
                $logger->info('Hook {hook} called', ['hook' => \HOOK_SMARTY_INC]);
            });
        }
    }

Der geloggte Text wird anschließend im Backend unter `Fehlerbehebung > Logbuch` angezeigt
und ist über das Auswahlmenü `Channel` filterbar.