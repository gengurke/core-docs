# HOOK_BESTELLUNGEN_XML_BEARBEITESTORNO (210)

## Triggerpunkt

Nach dem Abbrechen (Storno) einer Bestellung

## Parameter

* `JTL\Checkout\Bestellung` **&oBestellung** - Bestellungsobjekt
* `JTL\Customer\Kunde` **&oKunde** - Kundenobjekt
* `PaymentMethod` **oModule** - Zahlungsmodulobjekt