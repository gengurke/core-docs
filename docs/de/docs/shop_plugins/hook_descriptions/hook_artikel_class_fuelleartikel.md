# HOOK_ARTIKEL_CLASS_FUELLEARTIKEL (110)

## Triggerpunkt

Nach dem Laden eines Artikels

## Parameter

* `JTL\Catalog\Product\Artikel` **oArtikel** - geladener Artikel
* `array` **cacheTags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"