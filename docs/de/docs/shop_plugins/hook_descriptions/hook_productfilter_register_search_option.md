# HOOK_PRODUCTFILTER_REGISTER_SEARCH_OPTION (254)

## Triggerpunkt

Beim Registrieren der Suchfilter

## Parameter

* `JTL\Filter\SortingOptions\Factory` **factory** - Sortieroptionen-Objekt
* `JTL\Filter\ProductFilter` **productFilter** - Artikelfilter-Objekt