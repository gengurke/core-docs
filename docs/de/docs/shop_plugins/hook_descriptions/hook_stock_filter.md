# HOOK_STOCK_FILTER (221)

## Triggerpunkt

Am Ende des Lagerbestandsfilters

## Parameter

* `int` **conf** - Filtertyp
* `string` **&filterSQL** - Filter-SQL
* `bool` **withAnd** (ab 5.1.3) - AND / OR - Filter