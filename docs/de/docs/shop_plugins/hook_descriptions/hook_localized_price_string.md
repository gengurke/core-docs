# HOOK_LOCALIZED_PRICE_STRING (330)

## Triggerpunkt

Nach Erstellung der lokalisierten Preis-Strings

## Parameter

* `float|string` **price** - Der zu lokalisierende Preis
* `Currency` **currency** - Die genutzte Währung
* `bool` **html** - Schalter, ob Ausgabe als HTML-Entity erfolgt ist
* `int` **decimals** - Anzahl Dezimalstellen
* `string` **currencyName** - Der Name der genutzten Währung - ggf. als HTML-Entity
* `string` **localized** - Der neu formatierte Preis ohne Währungszeichen