# HOOK_FILTER_INC_BAUEARTIKELANZAHL (177)

## Triggerpunkt

Am Ende von `baueArtikelAnzahl()`

## Parameter

* `int` **&oAnzahl** - Artikelanzahl
* `stdClass` **&FilterSQL** - Artikelfilter
* `stdClass` **&oSuchergebnisse** - Ergebnisse der Suche
* `int` **&nArtikelProSeite** - Artikel pro angezeigter Seite
* `int` **nLimitN** - Limitierung der Suchergebnisse