# HOOK_BOXEN_INC_BESTSELLER (85)

## Triggerpunkt

Vor der Anzeige der "Best-Seller"-Artikel

## Parameter

* `JTL\Boxes\Items\BestsellingProducts` **&box** - "Best-Seller"-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"