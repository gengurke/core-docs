# HOOK_BOXEN_INC_TOPBEWERTET (92)

## Triggerpunkt

Vor der Anzeige der "Top bewertet"-Artikel

## Parameter

* `JTL\Boxes\Items\TopRatedProducts` **&box** - "Top-Rated-Products"-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"