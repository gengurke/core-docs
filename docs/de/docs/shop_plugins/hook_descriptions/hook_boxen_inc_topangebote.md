# HOOK_BOXEN_INC_TOPANGEBOTE (82)

## Triggerpunkt

Vor der Anzeige der Top-Angebote

## Parameter

* `JTL\Boxes\Item\TopOffers` **&sbox** - "Top-Angebote"-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"