# HOOK_BILDER_XML_BEARBEITEDELETES (206)

## Triggerpunkt

Vor dem Löschen von Bildern (in dbeS)

## Parameter

* `array` **Artikel** - Artikelbilder
* `array` **Kategorie** - Kategoriebilder
* `array` **KategoriePK** - Kategorie
* `array` **Eigenschaftswert** - Eigenschaftswertbilder
* `array` **Hersteller** - Hersteller
* `array` **Merkmal** - Merkmal
* `array` **Merkmalwert** - Merkmalwert