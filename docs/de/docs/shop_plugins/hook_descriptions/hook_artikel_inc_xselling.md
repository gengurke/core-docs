# HOOK_ARTIKEL_INC_XSELLING (57)

## Triggerpunkt

Vor der Rückgabe der Cross-Selling Artikel, in den Artikeldetails

## Parameter

* `int` **kArtikel** - die ID des aktuellen Artikels
* `stdClass` **&xSelling** - "Cross-Selling"-Objekt mit Artikelliste