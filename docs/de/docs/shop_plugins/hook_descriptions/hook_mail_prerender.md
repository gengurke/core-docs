# HOOK_MAIL_PRERENDER (290)

## Triggerpunkt

Vor dem Senden einer Email (bevor das Template gerendert wird)

## Parameter

* `\JTL\Mail\Mailer` **mailer** - Mailer-Objekt
* `\JTL\Mail\Mail` **mail** - Mail-Objekt