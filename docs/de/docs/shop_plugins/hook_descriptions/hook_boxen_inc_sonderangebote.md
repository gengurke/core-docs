# HOOK_BOXEN_INC_SONDERANGEBOTE (84)

## Triggerpunkt

Vor der Anzeige der Sonderangebote

## Parameter

* `JTL\Boxes\Item\SpecialOffers` **&box** - "Sonderangebote"-Objekt
* `array` **&cache_tags** - Umfang des Zwischenspeicherns
* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"