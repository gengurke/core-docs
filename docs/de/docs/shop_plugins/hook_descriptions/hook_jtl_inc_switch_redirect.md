# HOOK_JTL_INC_SWITCH_REDIRECT (98)

## Triggerpunkt

Nach dem setzen des Redirects, bevor Dieser in der Session gespeichert wird

## Parameter

* `int` **&cRedirect** - Redirect-ID
* `stdClass` **&oRedirect** - Redirect-Objekt