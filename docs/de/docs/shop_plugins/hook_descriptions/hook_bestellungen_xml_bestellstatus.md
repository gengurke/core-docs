# HOOK_BESTELLUNGEN_XML_BESTELLSTATUS (181)

## Triggerpunkt

Vor dem Aktualisieren des Bestellstatus

## Parameter

* `int` **&status** - Status der Bestellung
* `stdClass` **oBestellung** - aktuelle Bestellung