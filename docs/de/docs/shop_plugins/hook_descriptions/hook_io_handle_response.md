# HOOK_IO_HANDLE_RESPONSE (412)

## Triggerpunkt

Vor Rückgabe der Response in IOController::getResponse()

## Parameter

* `JTL\IO\IO` **&io** - IO-Objekt
* `Laminas\Diactoros\Response\JsonResponse` **&response** - Response-Objekt
