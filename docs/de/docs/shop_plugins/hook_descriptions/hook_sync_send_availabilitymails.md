# HOOK_SYNC_SEND_AVAILABILITYMAILS (336)

## Triggerpunkt

In `sendAvailabilityMails()`, in dbeS, vor dem Versenden von Verfügbarkeitsbenachrichtigungen

## Parameter

* `bool` **&sendMails** - Wenn auf FALSE gesetzt, werden keine Verfügbarkeitsbenachrichtigungen versendet
* `object` **product** - Der Artikel, für den Benachrichtigung versendet werden
* `array` **&subscriptions** - Referenz auf Liste von Einträgen aus tverfuegbarkeitsbenachrichtigung