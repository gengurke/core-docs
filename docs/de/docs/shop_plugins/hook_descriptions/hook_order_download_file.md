# HOOK_ORDER_DOWNLOAD_FILE (402)

## Triggerpunkt

Bevor eine Datei einer Bestellung heruntergeladen wird

## Parameter

* `JTL\Extensions\Download\Download` **download** - Download-Objekt
* `int` **customerID** - Kunden-ID
* `int` **orderID** - Bestellung-ID