# Programmier-Tipps

* [Mitmachen beim Entwickeln von JTL-Shop](contributing.md)
* [Informationen über das Erzeugen von Alerts (Meldungen wie z.B. Fehler/Hinweise)](alerts.md)
* [Notifications im Backend erzeugen](notifications.md)
* [Die zentrale Bedeutung der *Shop*-Klasse und ihre Benutzung](shopclass.md)
* [Blättern durch lange Listen ermöglichen](pagination.md)
* [Anzuzeigende Daten im Backend eingrenzen](backend_filter.md)
* [Debug-Informationen, Unterstützung bei der Entwicklung zur Laufzeit](debug.md)
* [Zeitmessungen in der Entwicklungsphase](profiling.md)
* [Erkennen von Crawler-Sessions](botsessions.md)
* [Konstanten der `config.JTL-Shop.ini.php`](config_tips.md)
* [Upgrade Guidelines](breaking_changes.md)
* [Lizenzen](licensing.md)
* [Patches einspielen](patches.md)
