# Artikelsticker

Sie können Artikelsticker, Template-spezifisch unter *Standardelemente->Artikelsticker* ein- oder ausschalten. Hierzu
setzen Sie den Wert bei `Sticker-Funktion aktiv` auf `Ja` oder `Nein`.

!!! important
    Die Einstellung `Sticker-Funktion aktiv` gilt nur für Ihr aktuell aktives Template und die ausgewählte Sprache.
    Wenn Sie mehrere Templates nutzen, müssen Sie diese Einstellung für jedes Template separat vornehmen.

!!! note
    Seit JTL Shop 5.3.0 können Sie mit dem Funktionsattribut `custom_item_badge` auch eigenes CSS im Artikelsticker
    verwenden. Sie können den Text und die Farben setzen, indem Sie beides in folgendem Format angeben:
    `SPRACHVARIABLE oder TEXT; TEXTFARBE; HINTERGRUNDFARBE`. Zum Beispiel
    `last_unit;#ffffff;#000000`, `special_offer;primary;secondary` oder `Top Angebot;white;danger`

## Typen und Inhalt

Sie können den Text der einzelnen Artikelsticker anpassen, indem Sie in Ihrem Shop-Backend unter *Sprachvariablen*
in der Sektion *productOverview* folgende Variablen anpassen:

| Variable | Inhalt |
| - | - |
| ribbon-1 | Bestseller |
| ribbon-2 | Sale %s |
| ribbon-3 | Neu |
| ribbon-4 | Top |
| ribbon-5 | Bald verfügbar |
| ribbon-6 | Top bewertet |
| ribbon-7 | Ausverkauft |
| ribbon-8 | Auf Lager |
| ribbon-9 | Vorbestellen |

!!! hint
    Bei Sprachvariablen können Sie den Platzhalter `%s` verwenden. Dieser wird beim Parsen des Templates durch einen
    bestimmten Wert ersetzt. In dem Fall der Variable `ribbon-2` wird der Platzhalter `%s` durch den Wert der
    Variable `$Artikel->Preise->discountPercentage` ersetzt. Dieser Wert wird automatisch berechnet und ist der
    Rabatt in Prozent, den Sie für den Artikel festgelegt haben.