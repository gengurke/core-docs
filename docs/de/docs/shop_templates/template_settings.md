# Template Einstellungen

## Was bewirken diese Einstellungen?

Mit diesen Einstellungen ist es möglich, verschiedene Teile des Templates schon beim Aufruf der Seite auf
unterschiedliche Art und Weise reagieren zu lassen.

Die Template-Einstellungen befinden sich im Shop-Backend unter ``Templates -> [Template-Name] -> Einstellungen``
und werden aus der, zum Template zugehörigen, ``template.xml`` generiert. Es befinden sich bereits einige
vordefinierte Einstellungen in der ``template.xml``. Sie können aber auch selbst Einstellungen hinzufügen,
vorzugsweise in einem eigenen :doc:`Child-Template </shop_templates/eigenes_template>`. In den Template-Einstellungen
können Sie festlegen, wie die Seite ausgegeben wird — zum Beispiel, welches Theme verwendet werden soll oder ob im
Footer Links angezeigt werden sollen und Einiges mehr.

## Beschreibung der einzelnen Einstellungen

Bei unserem Standard-Template werden einige vordefinierte Einstellungen mitgeliefert, deren Bedeutung im Folgenden
genauer erklärt wird.

### Allgemein

Komprimierung von JavaScript- und CSS-Dateien
: Wird diese Einstellung aktiviert, werden Javascript und CSS-Dateien komprimiert, um so die Dateigröße zu verringern
  und damit Traffic zu sparen.

### Theme

Theme
: Für das NOVA-Template gibt es aktuell die Themes clear (Standard), midnight und blackline.

Animation für Schaltflächen beim Hovern
: Damit wird eine kleine Animation auf Buttons wie z.B. dem Warrenkorb-Button aktiviert.

Animation für Wunschzettel und Vergleichliste
: Zeigt eine Animation wenn Artikel den entsprechenden Listen hinzugefügt werden.

Schaltfläche „Nach oben scrollen“ anzeigen
: Dem Kunden wird ein Link zum Anfang der Seite angeboten.

Sliderposition und Bannerposition
: Diese können sich direkt unter dem Header befinden (ganze Seitenbreite) oder im Contentbereich (Breite begrenzt).

Linke Seitenleiste
: Diese kann auf allen Seiten angezeigt werden oder nur in der Artikelliste (Kategorieseiten, Suchen etc.).

Favicon
: Ein Favicon ist ein kleines Bild (32x32, 16x16) welches in den Browser-Tabs neben dem Titel der Seite angezeigt
  wird.

### Header

Das NOVA-Template bietet vordefinierte Layouts für den Seitenkopf an. Sie können aus fünf verschiedenen
Darstellungsvarianten wählen. Zusätzlich haben sie die Möglichkeit ein eigenes Layout über die Experteneinstellungen
festzulegen.

!!! note
    Nutzen Sie gern eine der Voreinstellungen als Startpunkt für ihre Anpassungen. Klicken Sie auf das gewünschte
    Layout und legen Sie danach in den Detaileinstellungen ihre Änderungen fest.

### Megamenü

Kategorien
: Ist diese Option aktiv, werden alle Hauptkategorien des Shops im Megamenü dargestellt.
  Falls Sie diese Option deaktivieren, müssen Sie in der Boxenverwaltung eine Kategoriebox für jede Seite aktivieren,
  damit Ihre Kunden die Kategorien weiterhin erreichen.

Menü in aktueller Kategorie öffnen
: Öffnet ein Untermenü für die aktuelle Kategorie auf mobilen Geräten.

Kategoriebilder für erste Unterkategorie
: Diese Option bewirkt die Anzeige von Kategoriebildern anstelle von Kategorienamen auf Desktopgeräten.

Unterkategorien:
: Hiermit werden zusätzlich zu den Hauptkategorien auch die Unterkategorien angezeigt.

Seiten der Linkgruppe 'megamenu'
: Ist diese Option aktiviert, dann achtet das Megamenü auf eine Linkgruppe mit dem Namen ``megamenu`` und zeigt diese
  Links zusätzlich an. Diese Linkgruppe kann man unter ``Inhalte -> Eigene Seiten`` hinzufügen. Diese Seiten können
  dann im Megamenü hierarchisch aufgeklappt werden.

Hersteller-Dropdown
: Aktiviert einen zusätzlichen Menüpunkt im Megamenü, welcher eine Liste aller Hersteller anzeigt, die aktuell Artikel
  im Shop anbieten.

Herstellerbilder
: Es werden zu den Herstellern die entsprechenden Bilder angezeigt. Nur auf Desktopgeräten.

### Listen- und Galerieansicht

Filterposition
: Gibt an, ob Filtermöglichkeiten links in der Seitenleiste angezeigt oder in einem eigenen Fenster dargestellt
  werden.

Filtersuche ab X Filteroptionen anzeigen
: Erlaubt die Suche nach der gewünschten Filteroption, wenn mindestens so viele Filteroptionen wie hier angegeben
  vorliegen.

Filter initial immer ausklappen
: Gibt an, ob immer alle Filteroptionen der einzelnen Filterkategorien sichtbar sind oder diese erst ausgeklappt
  werden müssen.

Anzahl der initial sichtbaren Filteroptionen je Filter
: Legt fest, wie viele Filteroptionen standardmäßig pro Filter angezeigt werden. Weitere Filteroptionen können
  jederzeit zusätzlich eingeblendet werden.

Artikel in Warenkorb legen
: Blendet die Warenkorb-Schaltfläche in der Artikelvorschau der Listenansicht ein.

Anzahl der möglichen Variationen
: Gibt an, wie viele Variationen in der Listenansicht maximal angezeigt werden. Bei 0 werden keine Variationen
  angezeigt. Sollte ein Artikel mehr Variationen haben, so wird die Auswahl ausschließlich in den Artikeldetails
  angezeigt.

Anzahl der möglichen Variationswerte für Radiobuttons/Swatches (Liste)
: Gibt an, wie viele Variationswerte in der Listenansicht maximal für Radiobuttons und Swatches angezeigt werden.
  Dropdown-Menüs werden davon nicht beeinflusst. Sollte der Artikel mehr Werte haben, so wird die Auswahl
  ausschließlich in den Artikeldetails angezeigt.

Variationsvorschau anzeigen
: Gibt für Varkombi-Artikel an, wie viele Variationen in der Galerieansicht maximal angezeigt werden können.
  Bei 0 werden keine Variationen angezeigt. Sollte ein Artikel mehr Variationen haben, so wird die Auswahl
  ausschließlich in den Artikeldetails angezeigt.

Anzahl der möglichen Variationswerte für Radiobuttons/Swatches (Galerie)
: Gibt an, wie viele Variationswerte in der Galerieansicht maximal für Radiobuttons und Swatches angezeigt werden.
  Dropdown-Menüs werden davon nicht beeinflusst. Sollte der Artikel mehr Werte haben, so wird die Auswahl
  ausschließlich in den Artikeldetails angezeigt.

### Variationswertdarstellung

Slider für Swatches ab X Elementen
: Legt fest, ab wie vielen Elementen Swatches im Onlineshop über einen Slider dargestellt werden.

### Farben

Das Nova-template bietet die Möglichkeit verschiedene Farbvariablen direkt in den Template-Einstellungen zu verändern.
In diesem Bereich legen Sie mit wenigen Klicks z.B. Header- und Footerhintegrundfarben oder Schriftfarben fest.

### Benutzerdefinierte Styles

Für kleinere Anpassungen an dem jeweiligen Theme können Sie die Bereiche "Benutzerdefinierte Variablen" und
"Benutzerdefiniertes CSS, Sass oder Scss" nutzen.

### Footer-Einstellungen

Newsletter-Anmeldung im Footer
: Diese Einstellung blendet ein Eingabefeld für die Anmeldung zum Newsletter im Footer ein.
  Wenn Sie diese Option aktivieren, beachten Sie bitte auch die Einstellungen zum Newsletter!

Social-Media-Buttons im Footer
: Mit der Aktivierung dieser Einstellung wird für jede der folgenden Zeilen, die mit einem Link gefüllt sind, die
  entsprechende Social-Media-Schaltfläche im Footer eingeblendet.
  ```
    Facebook-Link   : ...
    Twitter-Link    :
    YouTube-Link    :
    Xing-Link       :
    LinkedIn-Link   :
    Vimeo-Link      :
    Instagram-Link  :
    Pinterest-Link  :
    Skype-Link      :
    TikTok-Link     :*
  ```