msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"

msgid "LastSearch_title"
msgstr "Letzte Suchanfragen"

msgid "LastSearch_desc"
msgstr " "

msgid "Search"
msgstr "Suche"

msgid "Search count"
msgstr "Gesuche"

msgid "Hits"
msgstr "Treffer"

msgid "No search queries found."
msgstr "Keine Suchanfragen vorhanden."

msgid "Language"
msgstr "Sprache"
